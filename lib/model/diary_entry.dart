class DiaryEntry {
  final String title;
  final double calories;
  final DateTime dateTime;
  final double balance;

  DiaryEntry({
    required this.title,
    required this.calories,
    required this.dateTime,
    required this.balance,
  });

  factory DiaryEntry.fromJson(Map<String, dynamic> json) => DiaryEntry(
        title: json['title'],
        calories: json['calories'].toDouble(),
        dateTime: DateTime.fromMillisecondsSinceEpoch(json['dateTime']),
        balance: json['balance'].toDouble(),
      );

  Map<String, dynamic> toJson() => {
        'title': title,
        'calories': calories,
        'dateTime': dateTime.millisecondsSinceEpoch,
        'balance': balance,
      };
}
