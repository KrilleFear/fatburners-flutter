import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:licaldia/config/app_constants.dart';
import 'package:licaldia/config/hive_keys.dart';
import 'package:licaldia/model/calorie_consumption.dart';
import 'package:licaldia/model/diary_entry.dart';
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:licaldia/model/notification_extension.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';

enum Sex {
  male,
  female,
  diverse,
}

enum DailyActivity {
  none,
  veryLittle,
  little,
  medium,
  much,
  veryMuch,
}

class Licaldia {
  final Box profileBox;
  final Box diaryBox;

  Licaldia(this.profileBox, this.diaryBox);

  Widget builder(BuildContext context, Widget? child) => Provider<Licaldia>(
        create: (_) => this,
        dispose: (_, licaldia) => licaldia.dispose(),
        child: child ?? Container(),
      );

  static Future<Licaldia> init() async {
    await Hive.initFlutter();
    final profileBox = await Hive.openBox(HiveKeys.profileBoxName);
    final diaryBox = await Hive.openBox(HiveKeys.diaryBoxName);
    final licaldia = Licaldia(profileBox, diaryBox);
    return licaldia;
  }

  Future<void> dispose() async {
    await profileBox.close();
    await diaryBox.close();
    return;
  }

  static Licaldia of(BuildContext context) =>
      Provider.of<Licaldia>(context, listen: false);

  bool get storeIsEmpty => profileBox.isEmpty && diaryBox.isEmpty;

  final onUpdate = StreamController<Licaldia>.broadcast();

  List<DiaryEntry> get diaryEntries => diaryBox.values
      .map((json) => DiaryEntry.fromJson(
            Map<String, dynamic>.from(json),
          ))
      .toList()
    ..sort((a, b) => a.dateTime.compareTo(b.dateTime));

  double get balance {
    if (diaryEntries.isEmpty) return 0;
    final lastEntry = diaryEntries.last;
    final start = lastEntry.balance;
    final millisecondsSinceStart = DateTime.now().millisecondsSinceEpoch -
        lastEntry.dateTime.millisecondsSinceEpoch;
    final caloriesPerMillisecond = dailyGoal / (24 * 60 * 60 * 1000);
    return start + (millisecondsSinceStart * caloriesPerMillisecond);
  }

  Future<void> addDiaryEntry(String title, double calories) async {
    await diaryBox.add(DiaryEntry(
      title: title,
      calories: calories,
      balance: balance - calories,
      dateTime: DateTime.now(),
    ).toJson());
    onUpdate.add(this);
  }

  Future<void> deleteLatestDiaryEntry() async {
    if (diaryEntries.last.calories == 0) return;
    await diaryBox.deleteAt(diaryEntries.length - 1);
    onUpdate.add(this);
  }

  Future<void> resetDiary() async {
    await profileBox.deleteAll(profileBox.keys);
    await diaryBox.deleteAll(diaryBox.keys).then((_) => onUpdate.add(this));
    return;
  }

  Future<void> restart() async {
    if (diaryEntries.isNotEmpty) {
      await addDiaryEntry('Start', 0);
    }
    onUpdate.add(this);
    return;
  }

  bool get breakfastNotification =>
      profileBox.get('breakfast.notification') ?? false;
  bool get lunchNotification => profileBox.get('lunch.notification') ?? false;
  bool get dinnerNotification => profileBox.get('dinner.notification') ?? false;

  TimeOfDay get breakfastNotificationTime => TimeOfDay(
        hour: profileBox.get('breakfast.hour') ?? 8,
        minute: profileBox.get('breakfast.minute') ?? 0,
      );
  TimeOfDay get lunchNotificationTime => TimeOfDay(
        hour: profileBox.get('lunch.hour') ?? 13,
        minute: profileBox.get('lunch.minute') ?? 0,
      );
  TimeOfDay get dinnerNotificationTime => TimeOfDay(
        hour: profileBox.get('dinner.hour') ?? 18,
        minute: profileBox.get('dinner.minute') ?? 0,
      );
  Future<void> setBreakfastNotificationTime(TimeOfDay time) async {
    await profileBox.put('breakfast.hour', time.hour);
    await profileBox.put('breakfast.minute', time.minute);
    await updateSheduledNotifications();
  }

  Future<void> setLunchNotificationTime(TimeOfDay time) async {
    await profileBox.put('lunch.hour', time.hour);
    await profileBox.put('lunch.minute', time.minute);
    await updateSheduledNotifications();
  }

  Future<void> setDinnerNotificationTime(TimeOfDay time) async {
    await profileBox.put('dinner.hour', time.hour);
    await profileBox.put('dinner.minute', time.minute);
    await updateSheduledNotifications();
  }

  Future<void> toggleBreakfastNotification(bool notify) async {
    await profileBox.put('breakfast.notification', notify);
    await updateSheduledNotifications();
  }

  Future<void> toggleLunchNotification(bool notify) async {
    await profileBox.put('lunch.notification', notify);
    await updateSheduledNotifications();
  }

  Future<void> toggleDinnerNotification(bool notify) async {
    await profileBox.put('dinner.notification', notify);
    await updateSheduledNotifications();
  }

  Sex get sex =>
      Sex.values.firstWhere((sex) => profileBox.get('sex') == sex.toString(),
          orElse: () => Sex.diverse);

  set sex(Sex newSex) =>
      profileBox.put('sex', newSex.toString()).then((_) => restart());

  /// The height in cm
  double get height => profileBox.get('height') ?? 170;

  set height(double newHeight) =>
      profileBox.put('height', newHeight).then((_) => restart());

  /// The weight in kilogram
  double get weight => profileBox.get('weight') ?? 70;

  set weight(double newWeight) =>
      profileBox.put('weight', newWeight).then((_) => restart());

  DailyActivity get dailyActivity => DailyActivity.values.firstWhere(
      (dailyActivity) =>
          profileBox.get('dailyActivity') == dailyActivity.toString(),
      orElse: () => DailyActivity.little);

  set dailyActivity(DailyActivity newDailyActivity) => profileBox
      .put('dailyActivity', newDailyActivity.toString())
      .then((_) => restart());

  /// The age in years
  int get age => profileBox.get('age') ?? 20;

  set age(int newAge) => profileBox.put('age', newAge).then((_) => restart());

  double get dailyGoal => profileBox.get('dailyGoal')?.toDouble() ?? dailyValue;

  set dailyGoal(double newDailyGoal) =>
      profileBox.put('dailyGoal', newDailyGoal).then((_) => onUpdate.add(this));

  double get coreValue => CalorieConsumption(
        age,
        weight,
        height,
        sex,
        dailyActivity,
      ).coreValue;

  double get dailyValue => CalorieConsumption(
        age,
        weight,
        height,
        sex,
        dailyActivity,
      ).dailyValue;

  Future<String> createBackup() async {
    final json = <String, dynamic>{
      'goal': dailyGoal,
      'sex': sex.toString(),
      'age': age,
      'height': height,
      'weight': weight,
      'dailyActivity': dailyActivity.toString(),
      'diary': diaryEntries.map((entry) => entry.toJson()).toList(),
    };
    final directory = Platform.isIOS
        ? await getApplicationDocumentsDirectory()
        : await getExternalStorageDirectory();
    final path =
        '${directory?.path ?? ''}/${AppConstants.appName}-backup-${DateTime.now().toIso8601String()}.json';
    final file = File(path);
    if (await file.exists()) await file.delete();
    await file.writeAsString(jsonEncode(json));
    return path;
  }

  Future<void> restoreFromBackup(Map<String, dynamic> backup) async {
    await resetDiary();
    dailyGoal = backup['goal'];
    sex = Sex.values.singleWhere((sex) => sex.toString() == backup['sex']);
    age = backup['age'];
    height = backup['height'];
    weight = backup['weight'];
    dailyActivity = DailyActivity.values.singleWhere(
        (dailyActivity) => dailyActivity.toString() == backup['dailyActivity']);
    final backupDiaryList = (backup['diary'] as List)
        .map((json) => DiaryEntry.fromJson(json))
        .toList();
    for (final entry in backupDiaryList) {
      await diaryBox.add(entry.toJson());
    }
    onUpdate.add(this);
    return;
  }
}
