import 'licaldia.dart';

class CalorieConsumption {
  int age;
  num weightInKilogram;
  num heightInCm;
  Sex sex;
  DailyActivity dailyActivity;

  CalorieConsumption(
    this.age,
    this.weightInKilogram,
    this.heightInCm,
    this.sex,
    this.dailyActivity,
  );

  double get coreValue {
    if (sex == Sex.female) {
      return 655.1 +
          (9.6 * weightInKilogram) +
          (1.80 * heightInCm) -
          (4.7 * age);
    }
    if (sex == Sex.male) {
      return 66.47 +
          (13.7 * weightInKilogram) +
          (5.00 * heightInCm) -
          (6.8 * age);
    }
    return (CalorieConsumption(
              age,
              weightInKilogram,
              heightInCm,
              Sex.male,
              dailyActivity,
            ).coreValue +
            CalorieConsumption(
              age,
              weightInKilogram,
              heightInCm,
              Sex.female,
              dailyActivity,
            ).coreValue) /
        2;
  }

  double get dailyValue {
    final coreValue = this.coreValue;
    switch (dailyActivity) {
      case DailyActivity.none:
        return coreValue * 0.95;
      case DailyActivity.veryLittle:
        return coreValue * 1.2;
      case DailyActivity.little:
        return coreValue * 1.5;
      case DailyActivity.medium:
        return coreValue * 1.7;
      case DailyActivity.much:
        return coreValue * 1.9;
      case DailyActivity.veryMuch:
        return coreValue * 2.2;
    }
  }
}
