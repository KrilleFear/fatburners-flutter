import 'package:flutter/material.dart';
import 'package:numberpicker/numberpicker.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

class NumberTextPickerDialog extends StatefulWidget {
  final BuildContext context;
  final String? title;
  final num startValue;
  final int minValue;
  final int maxValue;
  final bool decimal;
  final String? measurement;

  const NumberTextPickerDialog({
    Key? key,
    required this.context,
    this.title,
    required this.startValue,
    required this.minValue,
    required this.maxValue,
    this.decimal = false,
    this.measurement,
  }) : super(key: key);
  @override
  _NumberTextPickerDialogState createState() => _NumberTextPickerDialogState();
}

class _NumberTextPickerDialogState extends State<NumberTextPickerDialog> {
  num? value;
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: widget.title != null ? Text(widget.title!) : null,
      content: widget.decimal
          ? DecimalNumberPicker(
              minValue: widget.minValue,
              maxValue: widget.maxValue,
              value: value?.toDouble() ?? widget.startValue.toDouble(),
              onChanged: (n) => setState(() => value = n),
              decimalPlaces: 1,
              decimalTextMapper: (String? s) => '$s ${widget.measurement}',
              integerTextMapper: (String? s) => '$s.',
            )
          : NumberPicker(
              onChanged: (n) => setState(() => value = n),
              value: value?.round() ?? widget.startValue.round(),
              minValue: widget.minValue,
              maxValue: widget.maxValue,
              textMapper: (String? s) => '$s ${widget.measurement}',
            ),
      actions: [
        TextButton(
          onPressed: () => Navigator.of(context).pop<num>(null),
          child: Text(L10n.of(context)!.cancel),
        ),
        TextButton(
          onPressed: () => Navigator.of(context).pop<num>(value),
          child: Text(L10n.of(context)!.ok),
        ),
      ],
    );
  }
}
