import 'dart:async';

import 'package:licaldia/model/licaldia.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

class BalanceDisplay extends StatefulWidget {
  const BalanceDisplay({Key? key}) : super(key: key);

  @override
  _BalanceDisplayState createState() => _BalanceDisplayState();
}

class _BalanceDisplayState extends State<BalanceDisplay> {
  late final Timer timer;

  @override
  void initState() {
    timer = Timer.periodic(
        const Duration(milliseconds: 500), (timer) => setState(() {}));

    super.initState();
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final balance = Licaldia.of(context).balance;
    var balanceStr = ((balance * 100).round() / 100).toString();
    if (balanceStr.split('.').last.length == 1) {
      balanceStr += '0';
    }
    return Text('${L10n.of(context)!.yourBalance}: ' + balanceStr);
  }
}
