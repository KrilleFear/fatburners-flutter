import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:vrouter/vrouter.dart';

class AppBottomNavigationBar extends StatelessWidget {
  final int currentIndex;

  void onTap(int i, BuildContext context) {
    switch (i) {
      case 0:
        VRouter.of(context).to('/diary');
        break;
      case 1:
        VRouter.of(context).to('/profile');
        break;
      case 2:
        VRouter.of(context).to('/settings');
        break;
    }
  }

  const AppBottomNavigationBar({
    Key? key,
    required this.currentIndex,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return NavigationBar(
      selectedIndex: currentIndex,
      onDestinationSelected: (i) => onTap(i, context),
      destinations: [
        NavigationDestination(
          icon: const Icon(Icons.history_outlined),
          selectedIcon: const Icon(Icons.history),
          label: L10n.of(context)!.diary,
        ),
        NavigationDestination(
          icon: const Icon(Icons.account_circle_outlined),
          selectedIcon: const Icon(Icons.account_circle),
          label: L10n.of(context)!.profile,
        ),
        NavigationDestination(
          icon: const Icon(Icons.settings_outlined),
          selectedIcon: const Icon(Icons.settings),
          label: L10n.of(context)!.settings,
        ),
      ],
    );
  }
}
