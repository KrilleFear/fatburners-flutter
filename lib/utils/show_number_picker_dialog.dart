import 'package:licaldia/widgets/dialogs/number_text_picker_dialog.dart';
import 'package:flutter/material.dart';

Future<num?> showNumberPickerDialog({
  required BuildContext context,
  String? title,
  required num value,
  required int minValue,
  required int maxValue,
  bool decimal = false,
  String? measurement,
}) =>
    showDialog<num>(
      context: context,
      builder: (context) => NumberTextPickerDialog(
        context: context,
        startValue: value,
        minValue: minValue,
        maxValue: maxValue,
        decimal: decimal,
        measurement: measurement,
      ),
    );
