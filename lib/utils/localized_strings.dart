import 'package:licaldia/model/licaldia.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

extension LocalizedSexExtension on Sex {
  String toLocalizedString(BuildContext context) {
    switch (this) {
      case Sex.male:
        return L10n.of(context)!.male;
      case Sex.female:
        return L10n.of(context)!.female;
      case Sex.diverse:
        return L10n.of(context)!.diverse;
    }
  }
}
