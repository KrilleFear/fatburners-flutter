import 'package:licaldia/config/app_constants.dart';
import 'package:flutter/material.dart';
import 'package:licaldia/model/licaldia.dart';
import 'package:vrouter/vrouter.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';

import 'config/app_routes.dart';

void main() async => runApp(LicaldiaApp(await Licaldia.init()));

class LicaldiaApp extends StatelessWidget {
  final Licaldia licaldia;
  const LicaldiaApp(this.licaldia, {Key? key}) : super(key: key);

  static final ThemeData theme = ThemeData(
    useMaterial3: true,
    colorSchemeSeed: Colors.grey,
  );

  @override
  Widget build(BuildContext context) {
    return VRouter(
      title: AppConstants.appName,
      localizationsDelegates: L10n.localizationsDelegates,
      supportedLocales: L10n.supportedLocales,
      theme: theme,
      darkTheme: theme,
      routes: AppRoutes.routes,
      builder: licaldia.builder,
    );
  }
}
