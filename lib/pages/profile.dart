import 'dart:math';

import 'package:licaldia/model/bmi_counter.dart';
import 'package:licaldia/model/licaldia.dart';
import 'package:licaldia/utils/show_number_picker_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

import 'views/profile_view.dart';

class Profile extends StatefulWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  ProfileController createState() => ProfileController();
}

class ProfileController extends State<Profile> {
  Sex get sexValue => Licaldia.of(context).sex;
  void setSex(Sex? newSex) {
    if (newSex == null) return;
    Licaldia.of(context).sex = newSex;
    return;
  }

  int get ageValue => Licaldia.of(context).age;
  void setAge(int? newAge) {
    if (newAge == null) return;
    Licaldia.of(context).age = newAge;
    return;
  }

  double get minDailyGoal => max(Licaldia.of(context).dailyValue - 2000, 500);
  double get maxDailyGoal => min(Licaldia.of(context).dailyValue + 2000, 5000);

  double? _dailyGoal;
  double get dailyGoal {
    final goal = _dailyGoal ?? Licaldia.of(context).dailyGoal;
    if (goal > maxDailyGoal) {
      setDailyGoal(maxDailyGoal);
      return maxDailyGoal;
    }
    if (goal < minDailyGoal) {
      setDailyGoal(minDailyGoal);
      return minDailyGoal;
    }
    return goal;
  }

  String get bmi =>
      BmiCounter(Licaldia.of(context).height, Licaldia.of(context).weight)
          .roundedBmi
          .toString();

  String get bmiStatus {
    final category =
        BmiCounter(Licaldia.of(context).height, Licaldia.of(context).weight)
            .category;
    switch (category) {
      case BmiCategory.severeUnderweightGrade2:
        return L10n.of(context)!.severeUnderweightGrade2;
      case BmiCategory.severeUnderweightGrade1:
        return L10n.of(context)!.severeUnderweightGrade1;
      case BmiCategory.moderatelyUnderweight:
        return L10n.of(context)!.moderatelyUnderweight;
      case BmiCategory.slightlyUnderweight:
        return L10n.of(context)!.slightlyUnderweight;
      case BmiCategory.normalWeight:
        return L10n.of(context)!.normalWeight;
      case BmiCategory.preObesity:
        return L10n.of(context)!.preObesity;
      case BmiCategory.grade1Obesity:
        return L10n.of(context)!.grade1Obesity;
      case BmiCategory.grade2Obesity:
        return L10n.of(context)!.grade2Obesity;
      case BmiCategory.grade3Obesity:
        return L10n.of(context)!.grade3Obesity;
    }
  }

  String dailyActivityLocalized(DailyActivity dailyActivity) {
    switch (dailyActivity) {
      case DailyActivity.none:
        return L10n.of(context)!.none;
      case DailyActivity.veryLittle:
        return L10n.of(context)!.veryLittle;
      case DailyActivity.little:
        return L10n.of(context)!.little;
      case DailyActivity.medium:
        return L10n.of(context)!.medium;
      case DailyActivity.much:
        return L10n.of(context)!.much;
      case DailyActivity.veryMuch:
        return L10n.of(context)!.veryMuch;
    }
  }

  DailyActivity get dailyActivityValue => Licaldia.of(context).dailyActivity;
  void setDailyActivity(DailyActivity? newDailyActivity) {
    if (newDailyActivity == null) return;
    Licaldia.of(context).dailyActivity = newDailyActivity;
    return;
  }

  Color get bmiColor {
    final category =
        BmiCounter(Licaldia.of(context).height, Licaldia.of(context).weight)
            .category;
    switch (category) {
      case BmiCategory.severeUnderweightGrade2:
        return Colors.red[900]!;
      case BmiCategory.severeUnderweightGrade1:
        return Colors.red[800]!;
      case BmiCategory.moderatelyUnderweight:
        return Colors.red[700]!;
      case BmiCategory.slightlyUnderweight:
        return Colors.orange;
      case BmiCategory.normalWeight:
        return Colors.green;
      case BmiCategory.preObesity:
        return Colors.orange;
      case BmiCategory.grade1Obesity:
        return Colors.red[700]!;
      case BmiCategory.grade2Obesity:
        return Colors.red[800]!;
      case BmiCategory.grade3Obesity:
        return Colors.red[900]!;
    }
  }

  void changeDailyGoal(double? newGoal) => setState(() => _dailyGoal = newGoal);
  void setDailyGoal(double newGoal) {
    if (newGoal < 500 || newGoal > 5000) {
      setState(() => _dailyGoal = null);
      return;
    }
    Licaldia.of(context).dailyGoal = newGoal;
    return;
  }

  String get dailyGoalLocalized {
    final dailyValue = Licaldia.of(context).dailyValue.round();
    if (dailyGoal.round() == dailyValue) {
      return L10n.of(context)!.holdWeight;
    }
    if (dailyGoal.round() < dailyValue) {
      return L10n.of(context)!.weightLoss;
    }
    return L10n.of(context)!.weightGain;
  }

  Color get dailyGoalColor {
    final dailyValue = Licaldia.of(context).dailyValue.round();
    if (dailyGoal.round() == dailyValue) {
      return Colors.green;
    }
    if (dailyGoal.round() < dailyValue) {
      return Colors.orange;
    }
    return Colors.blue;
  }

  void resetDailyGoal() {
    setState(() => _dailyGoal = null);
    Licaldia.of(context).dailyGoal = Licaldia.of(context).dailyValue;
  }

  String dailyBalanceLocalized([double? goal]) {
    final balance =
        ((goal ?? dailyGoal) - Licaldia.of(context).dailyValue).round();
    return '${L10n.of(context)!.dailyBalance}: ${balance}Kcal';
  }

  void setHeight() async {
    final newHeight = await showNumberPickerDialog(
      title: L10n.of(context)!.newFoodEntry,
      context: context,
      minValue: 30,
      maxValue: 300,
      value: Licaldia.of(context).height,
      measurement: 'cm',
      decimal: true,
    );
    if (newHeight == null) return;
    Licaldia.of(context).height = newHeight.toDouble();
    return;
  }

  void setWeight() async {
    final newWeight = await showNumberPickerDialog(
      title: L10n.of(context)!.newFoodEntry,
      context: context,
      minValue: 30,
      maxValue: 300,
      value: Licaldia.of(context).weight,
      measurement: 'Kg',
      decimal: true,
    );
    if (newWeight == null) return;
    Licaldia.of(context).weight = newWeight.toDouble();
    return;
  }

  String get currentHeight =>
      '${L10n.of(context)!.bodySize}: ${Licaldia.of(context).height}cm';
  String get currentWeight =>
      '${L10n.of(context)!.weight}: ${Licaldia.of(context).weight}Kg';

  @override
  Widget build(BuildContext context) {
    return ProfileView(this);
  }
}
