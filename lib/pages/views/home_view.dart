import 'package:licaldia/pages/home.dart';
import 'package:flutter/material.dart';

class HomeView extends StatelessWidget {
  final HomeController controller;

  const HomeView(this.controller, {Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return const Scaffold(body: Center(child: CircularProgressIndicator()));
  }
}
