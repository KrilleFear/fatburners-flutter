import 'package:licaldia/model/licaldia.dart';
import 'package:licaldia/widgets/app_bottom_navigation_bar.dart';
import 'package:licaldia/widgets/title_list_tile.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

import '../settings.dart';

class SettingsView extends StatelessWidget {
  final SettingsController controller;

  const SettingsView(this.controller, {Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text(L10n.of(context)!.settings),
      ),
      body: ListView(
        children: [
          TitleListTile(label: L10n.of(context)!.notifications),
          SwitchListTile.adaptive(
            value: Licaldia.of(context).breakfastNotification,
            onChanged: controller.toggleBreakfastNotification,
            title: Text(L10n.of(context)!.breakfastReminder),
            subtitle: Align(
              alignment: Alignment.centerLeft,
              child: TextButton(
                onPressed: controller.changeBreakfastTime,
                child: Text(Licaldia.of(context)
                    .breakfastNotificationTime
                    .format(context)),
              ),
            ),
          ),
          SwitchListTile.adaptive(
            value: Licaldia.of(context).lunchNotification,
            onChanged: controller.toggleLunchNotification,
            title: Text(L10n.of(context)!.lunchReminder),
            subtitle: Align(
              alignment: Alignment.centerLeft,
              child: TextButton(
                onPressed: controller.changeLunchTime,
                child: Text(
                    Licaldia.of(context).lunchNotificationTime.format(context)),
              ),
            ),
          ),
          SwitchListTile.adaptive(
            value: Licaldia.of(context).dinnerNotification,
            onChanged: controller.toggleDinnerNotification,
            title: Text(L10n.of(context)!.dinnerReminder),
            subtitle: Align(
              alignment: Alignment.centerLeft,
              child: TextButton(
                onPressed: controller.changeDinnerTime,
                child: Text(Licaldia.of(context)
                    .dinnerNotificationTime
                    .format(context)),
              ),
            ),
          ),
          TitleListTile(label: L10n.of(context)!.about),
          ListTile(
            trailing: const Icon(Icons.help_outlined),
            title: Text(L10n.of(context)!.help),
            onTap: controller.helpAction,
          ),
          ListTile(
            trailing: const Icon(Icons.source_outlined),
            title: Text(L10n.of(context)!.sourcecode),
            onTap: controller.sourceCodeAction,
          ),
          ListTile(
            trailing: const Icon(Icons.info_outlined),
            title: Text(L10n.of(context)!.about),
            onTap: controller.aboutAction,
          ),
          TitleListTile(label: L10n.of(context)!.data),
          ListTile(
            trailing: const Icon(Icons.save),
            title: Text(L10n.of(context)!.backupAllData),
            onTap: controller.createBackupAction,
          ),
          ListTile(
            trailing: const Icon(Icons.restore_outlined),
            title: Text(L10n.of(context)!.restoreFromBackup),
            onTap: controller.restoreBackupAction,
          ),
          ListTile(
            trailing: const Icon(Icons.delete_outlined),
            title: Text(L10n.of(context)!.clearAll),
            onTap: controller.clearAllAction,
          ),
        ],
      ),
      bottomNavigationBar: const AppBottomNavigationBar(currentIndex: 2),
    );
  }
}
