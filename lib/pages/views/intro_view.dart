import 'package:licaldia/config/app_assets.dart';
import 'package:licaldia/pages/intro.dart';
import 'package:flutter/material.dart';
import 'package:intro_views_flutter/intro_views_flutter.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

class IntroView extends StatelessWidget {
  final IntroController controller;

  const IntroView(this.controller, {Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return IntroViewsFlutter(
      [
        PageViewModel(
          pageColor: const Color(0xFF03A9F4),
          body: Text(L10n.of(context)!.introBody1),
          mainImage: Image.asset(AppAssets.intro1),
          title: Text(L10n.of(context)!.introTitle1),
          titleTextStyle: const TextStyle(
            color: Colors.white,
            fontSize: 24,
          ),
          bodyTextStyle: const TextStyle(color: Colors.white),
        ),
        PageViewModel(
          pageColor: const Color(0xFF8BC34A),
          body: Text(L10n.of(context)!.introBody2),
          mainImage: Image.asset(AppAssets.intro2),
          title: Text(L10n.of(context)!.introTitle2),
          titleTextStyle: const TextStyle(
            color: Colors.white,
            fontSize: 24,
          ),
          bodyTextStyle: const TextStyle(color: Colors.white),
        ),
        PageViewModel(
          mainImage: Image.asset(AppAssets.intro3),
          pageBackground: Container(
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                stops: [0.0, 1.0],
                begin: FractionalOffset.topCenter,
                end: FractionalOffset.bottomCenter,
                tileMode: TileMode.repeated,
                colors: [
                  Colors.orange,
                  Colors.pinkAccent,
                ],
              ),
            ),
          ),
          body: Text(L10n.of(context)!.introBody3),
          title: Text(L10n.of(context)!.introTitle3),
          titleTextStyle: const TextStyle(
            color: Colors.white,
            fontSize: 24,
          ),
          bodyTextStyle: const TextStyle(
            color: Colors.white,
            fontSize: 20,
          ),
        ),
      ],
      showNextButton: true,
      showBackButton: true,
      onTapDoneButton: controller.doneAction,
      backText: Text(L10n.of(context)!.back),
      doneText: Text(L10n.of(context)!.done),
      nextText: Text(L10n.of(context)!.next),
      pageButtonTextStyles: const TextStyle(
        color: Colors.white,
        fontSize: 14.0,
      ),
    );
  }
}
