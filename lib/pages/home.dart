import 'package:licaldia/model/licaldia.dart';
import 'package:licaldia/pages/views/home_view.dart';
import 'package:flutter/material.dart';
import 'package:vrouter/vrouter.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  HomeController createState() => HomeController();
}

class HomeController extends State<Home> {
  void _init() {
    if (Licaldia.of(context).storeIsEmpty) {
      VRouter.of(context).to('/intro');
    } else {
      VRouter.of(context).to('/diary');
    }
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) => _init());
  }

  @override
  Widget build(BuildContext context) => HomeView(this);
}
