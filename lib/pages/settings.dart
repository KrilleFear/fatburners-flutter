import 'dart:convert';
import 'dart:io';

import 'package:licaldia/config/app_constants.dart';
import 'package:licaldia/model/licaldia.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:share_plus/share_plus.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

import 'views/settings_view.dart';

class Settings extends StatefulWidget {
  const Settings({Key? key}) : super(key: key);

  @override
  SettingsController createState() => SettingsController();
}

class SettingsController extends State<Settings> {
  void toggleBreakfastNotification(bool notify) async {
    await Licaldia.of(context).toggleBreakfastNotification(notify);
    setState(() {});
  }

  void toggleDinnerNotification(bool notify) async {
    await Licaldia.of(context).toggleDinnerNotification(notify);
    setState(() {});
  }

  void toggleLunchNotification(bool notify) async {
    await Licaldia.of(context).toggleLunchNotification(notify);
    setState(() {});
  }

  void changeBreakfastTime() async {
    final time = await showTimePicker(
      context: context,
      initialTime: Licaldia.of(context).breakfastNotificationTime,
    );
    if (time == null) return;
    Licaldia.of(context).setBreakfastNotificationTime(time);
    setState(() {});
  }

  void changeDinnerTime() async {
    final time = await showTimePicker(
      context: context,
      initialTime: Licaldia.of(context).dinnerNotificationTime,
    );
    if (time == null) return;
    Licaldia.of(context).setDinnerNotificationTime(time);
    setState(() {});
  }

  void changeLunchTime() async {
    final time = await showTimePicker(
      context: context,
      initialTime: Licaldia.of(context).lunchNotificationTime,
    );
    if (time == null) return;
    Licaldia.of(context).setLunchNotificationTime(time);
    setState(() {});
  }

  void helpAction() => launch(AppConstants.helpUrl);
  void sourceCodeAction() => launch(AppConstants.sourceCodeUrl);
  void aboutAction() => showAboutDialog(
        context: context,
        applicationName: AppConstants.appName,
      );
  void clearAllAction() async {
    final consent = await showDialog<bool>(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(L10n.of(context)!.areYouSure),
        content: Text(L10n.of(context)!.thisWillDeleteAllData),
        actions: [
          TextButton(
            onPressed: () => Navigator.of(context).pop<bool>(false),
            child: Text(L10n.of(context)!.cancel),
          ),
          TextButton(
            onPressed: () => Navigator.of(context).pop<bool>(true),
            child: Text(
              L10n.of(context)!.clearAll,
              style: const TextStyle(color: Colors.red),
            ),
          ),
        ],
      ),
    );
    if (consent != true) return;
    await Licaldia.of(context).resetDiary();
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(L10n.of(context)!.allDataCleared),
      ),
    );
  }

  void createBackupAction() async {
    final path = await Licaldia.of(context).createBackup();
    await Share.shareFiles([path], text: 'Licaldia backup');
  }

  void restoreBackupAction() async {
    final pickedFile = await FilePicker.platform
        .pickFiles(type: FileType.custom, allowedExtensions: ['json']);
    if (pickedFile == null) return;
    final path = pickedFile.files.single.path;
    if (path == null) return;
    try {
      final json = jsonDecode(await File(path).readAsString());
      await Licaldia.of(context).restoreFromBackup(json);
    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(e.toString()),
        ),
      );
      rethrow;
    }
  }

  @override
  Widget build(BuildContext context) => SettingsView(this);
}
