import 'package:flutter/material.dart';
import 'package:vrouter/vrouter.dart';

import 'views/intro_view.dart';

class Intro extends StatefulWidget {
  const Intro({Key? key}) : super(key: key);

  @override
  IntroController createState() => IntroController();
}

class IntroController extends State<Intro> {
  void doneAction() => VRouter.of(context).to('/diary');

  @override
  Widget build(BuildContext context) => IntroView(this);
}
