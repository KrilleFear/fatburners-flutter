# Licaldia

Track your calories easily.

Licaldia lets you track your calories to help you to lose or gain weight. 

## How to build

Just setup [Flutter](https://flutter.dev), connect your device and run with:

```bash
flutter run
```